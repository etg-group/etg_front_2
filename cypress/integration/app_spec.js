describe('ET game', () => {
  beforeEach(() => {
    // visit our local server running the application
    //
    // visiting before each test ensures the app
    // gets reset to a clean state
    //
    // https://on.cypress.io/api/visit
    cy.server()
    cy.route('GET', '/api/mapmaker', 'fixture:map.json')
    cy.route('GET', '/api/highscore', 'fixture:highscores.json')
    cy.visit('http://localhost:2000')
  })

  it('should show ranking on landing page', () => {
    cy.get('#highscores').should('contain', '1')
    cy.get('#highscores').should('contain', 'Fin')
    cy.get('#highscores').should('contain', '28600')
  })
})
