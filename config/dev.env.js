'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  HIGHSCORE_URL: '"http://etg-balancer-862594806.eu-central-1.elb.amazonaws.com:4000/api/highscore"',
  MAPMAKER_URL: '"http://etg-balancer-862594806.eu-central-1.elb.amazonaws.com:3000/api/mapmaker"'
})
