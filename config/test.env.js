'use strict'
const merge = require('webpack-merge')
const devEnv = require('./dev.env')

module.exports = merge(devEnv, {
  NODE_ENV: '"testing"',
  HIGHSCORE_URL: '"http://localhost:4000/api/highscore"',
  MAPMAKER_URL: '"http://localhost:3000/api/mapmaker"'
})
