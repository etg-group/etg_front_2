'use strict'
module.exports = {
  NODE_ENV: '"production"',
  HIGHSCORE_URL: '"http://etg-balancer-862594806.eu-central-1.elb.amazonaws.com:4000/api/highscore"',
  MAPMAKER_URL: '"http://etg-balancer-862594806.eu-central-1.elb.amazonaws.com:3000/api/mapmaker"'
}
