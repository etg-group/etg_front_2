import { mount } from 'vue-test-utils'
import NotFound from '@/components/NotFound'
import { expect } from 'chai'

test('shallow NotFound page opens', () => {
  const wrapper = mount(NotFound)
  expect(wrapper.find('div#welcome').text()).to.have.string('This page does not exist.')
})
