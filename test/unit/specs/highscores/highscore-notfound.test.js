import { shallow } from 'vue-test-utils'
import Index from '@/components/Index'
import Highscore from '@/components/Highscore'
import { expect } from 'chai'
jest.mock('@/assets/js/HighscoreService', () => ({
  getHighscores: (errorCallback, successCallback) => {
    errorCallback('Error')
  }
}))

test('error message is shown if no highscores are found', () => {
  const wrapper = shallow(Index, {
    stubs: {
      'highscore': Highscore,
      'welcome': true
    }
  })
  expect(wrapper.find('div.error').text()).to.have.string('Error')
})
