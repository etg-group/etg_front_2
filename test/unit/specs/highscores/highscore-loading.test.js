import { shallow } from 'vue-test-utils'
import Index from '@/components/Index'
import Highscore from '@/components/Highscore'
import { expect } from 'chai'
jest.mock('@/assets/js/HighscoreService', () => ({
  getHighscores: (errorCallback, successCallback) => {
    successCallback(undefined)
  }
}))

test('loading message is shown while highscores are not found', () => {
  const wrapper = shallow(Index, {
    stubs: {
      'highscore': Highscore,
      'welcome': true
    }
  })
  expect(wrapper.find('div').text()).to.have.string('Loading highscores....')
})
