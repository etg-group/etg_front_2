import { shallow } from 'vue-test-utils'
import Index from '@/components/Index'
import Highscore from '@/components/Highscore'
import { expect } from 'chai'
jest.mock('@/assets/js/HighscoreService', () => ({
  getHighscores: (errorCallback, successCallback) => {
    successCallback([{
      name: 'Fin',
      score: '10000',
      date: '2017-10-20 12:30:56'
    }])
  }
}))

test('highscores are shown on landingpage', () => {
  const wrapper = shallow(Index, {
    stubs: {
      'highscore': Highscore,
      'welcome': true
    }
  })
  expect(wrapper.find('div#highscores').text()).to.have.string('Fin')
  expect(wrapper.find('div#highscores').text()).to.have.string('10000')
})
