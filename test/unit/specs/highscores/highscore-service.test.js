import { prettifyData } from '@/assets/js/HighscoreService.js'
import { expect } from 'chai'

test('dates are formatted as [DAY] [MONTHNAME] [YEAR] [LOCALTIME]', () => {
  const testData = [{
    name: 'Fin',
    score: '1000',
    date: '2017-10-11 13:58:17'
  }]
  const newData = prettifyData(testData)
  expect(newData[0].date).to.equal('11 October 2017 1:58 pm')
})
