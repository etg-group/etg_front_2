import { mount } from 'vue-test-utils'
import Countdown from '@/components/Countdown/Countdown'
import { expect } from 'chai'
import $router from '../../utils/mock-router'
import $route from '../../utils/mock-route'

test('Countdown shows a hint', () => {
  const $route = {
    params: {
      controls: 'keyboard'
    }
  }
  const wrapper = mount(Countdown, {
    mocks: {
      $route
    }
  })
  expect(wrapper.find('p#hint').text()).to.exist
})

test('Countdown triggers game', () => {
  jest.useFakeTimers()
  $route.params.controls = 'keyboard'
  const routerSpy = jest.spyOn($router, 'push')
  mount(Countdown, {
    mocks: {
      $route,
      $router
    }
  })
  expect(routerSpy.mock.calls.length).to.equal(0)
  jest.runAllTimers()
  expect(routerSpy.mock.calls.length).to.equal(1)
  expect(routerSpy.mock.calls[0][0]).to.equal('/Game/keyboard')
})
