import { getHint } from '@/assets/js/HintService.js'
import { expect } from 'chai'

test('Hints can be retrieved', () => {
  const newHint = getHint()
  expect(newHint).to.exist
})
