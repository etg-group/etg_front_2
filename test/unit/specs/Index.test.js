import { shallow, mount } from 'vue-test-utils'
import Index from '@/components/Index'
import Highscore from '@/components/Highscore'
import { expect } from 'chai'

test('shallow Index with highscore', () => {
  const wrapper = shallow(Index, {
    stubs: {
      'highscore': Highscore,
      'welcome': true
    }
  })
  expect(wrapper.contains('div#index')).to.be.true
  expect(wrapper.contains('highscore')).to.be.true
  expect(wrapper.contains('welcome')).to.be.false
})

test('Mount Index with Highscore stubbed', () => {
  const wrapper = mount(Index, {
    stubs: ['highscore']
  })
  expect(wrapper.contains('div#index')).to.be.true
  expect(wrapper.contains('controls')).to.be.true
  expect(wrapper.contains('highscore')).to.be.false
})
