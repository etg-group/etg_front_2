import { getHighscores, saveHighscore } from '../../../src/assets/js/HighscoreService.js'
import chai from 'chai'
import path from 'path'
import pact from 'pact'
import chaiAsPromised from 'chai-as-promised'

const MOCK_SERVER_PORT = 4000
const expect = chai.expect
chai.use(chaiAsPromised)

describe('highscore api', () => {
  const provider = pact({
    consumer: 'ExploratoryTestingGame',
    provider: 'HighscoreApi',
    log: path.resolve(process.cwd(), 'logs', 'pact.log'),
    dir: path.resolve(process.cwd(), 'pacts'),
    logLevel: 'INFO',
    port: MOCK_SERVER_PORT,
    spec: 2
  })

  context('when the pact mock is running', () => {
    describe('getting highscores: ', () => {
      before((done) => {
        provider.setup()
          .then(() => {
            provider.addInteraction({
              state: 'I have a record of Fin with 1000 points',
              uponReceiving: 'a request for highscore',
              withRequest: {
                method: 'GET',
                path: '/api/highscore'
              },
              willRespondWith: {
                status: 200,
                body: pact.Matchers.eachLike({
                  name: pact.Matchers.somethingLike('Klaas'),
                  score: pact.Matchers.somethingLike(1000),
                  date: pact.Matchers.somethingLike('2017-10-11 12:35:32')
                })
              }
            })
            provider.addInteraction({
              uponReceiving: 'request to add a new highscore',
              withRequest: {
                method: 'put',
                path: '/api/highscore',
                headers: {
                  'name': 'Klaas',
                  'score': '1000'
                }
              },
              willRespondWith: {
                status: 204
              }
            })
          })
          .then(() => done())
      })

      it('I should be able to retrieve the top 10 highscores', (done) => {
        getHighscores(undefined, (data) => {
          expect(data[0]['name']).to.equal('Klaas')
          expect(data[0]['score']).to.equal(1000)
          done()
        })
      })

      it('I should be able to store a new highscore', (done) => {
        saveHighscore('Klaas', 1000, (error) => {
          console.log('errror: ' + error)
          done()
        }, (success) => {
          done()
        })
      })

      after((done) => {
        provider.verify().then(() => {
          provider.finalize().then(() => {
            done()
          })
        })
      })
    })
  })
})
