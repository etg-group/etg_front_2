import { generateMap } from '../../../src/assets/js/MapmakerService.js'
import chai from 'chai'
import path from 'path'
import pact from 'pact'
import chaiAsPromised from 'chai-as-promised'

const MOCK_SERVER_PORT = 3000
const expect = chai.expect
chai.use(chaiAsPromised)

describe('highscore api', () => {
  const provider = pact({
    consumer: 'ExploratoryTestingGame',
    provider: 'MapMakerApi',
    log: path.resolve(process.cwd(), 'logs', 'pact.log'),
    dir: path.resolve(process.cwd(), 'pacts'),
    logLevel: 'INFO',
    port: MOCK_SERVER_PORT,
    spec: 2
  })

  context('when the pact mock is running', () => {
    describe('getting map: ', () => {
      before((done) => {
        provider.setup()
          .then(() => {
            provider.addInteraction({
              uponReceiving: 'request for an awesome map',
              withRequest: {
                method: 'get',
                path: '/api/mapmaker',
                headers: {
                  'features': '20',
                  'bugs': '5'
                }
              },
              willRespondWith: {
                status: 200,
                body: {
                  'posY0': {
                    'posX0': {
                      'pathRight': pact.Matchers.term({matcher: '^(Working|Broken)$', generate: 'Working'}),
                      'pathDown': pact.Matchers.term({matcher: '^(Working|Broken)$', generate: 'Working'})
                    },
                    'posX1': {
                      'pathRight': pact.Matchers.term({matcher: '^(Working|Broken)$', generate: 'Working'}),
                      'pathDown': pact.Matchers.term({matcher: '^(Open|Feature)$', generate: 'Open'})
                    }
                  }
                }
              }
            })
          })
          .then(() => done())
      })

      it('I should be able to retrieve a fully generated map', (done) => {
        generateMap((error) => {
          throw new Error(error)
        }, (mapData) => {
          expect(mapData['posY0']['posX0']['pathRight']).to.equal('Working')
          done()
        })
      })

      after((done) => {
        provider.verify().then(() => {
          provider.finalize().then(() => {
            provider.removeInteractions()
            done()
          })
        })
      })
    })
  })
})
