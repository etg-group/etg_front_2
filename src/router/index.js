import Vue from 'vue'
import VueRouter from 'vue-router'

import NotFound from '../components/NotFound'
import Home from '../components/Index'
import Countdown from '../components/Countdown/Countdown'
import Game from '../components/Game/Index'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  base: __dirname,
  routes: [
    { path: '/', component: Home },
    { path: '/Countdown/:controls', component: Countdown },
    { path: '/Game/:controls', component: Game },
    { path: '*', component: NotFound }
  ]
})

export default router
