import Vue from 'vue'
import VueResource from 'vue-resource'

Vue.use(VueResource)

const url = process.env.MAPMAKER_URL

export function generateMap (errorCallback, successCallback) {
  function handleData (rawData) {
    successCallback(rawData.body)
  }

  const headers = {
    headers: {
      'features': '20',
      'bugs': '5'
    }
  }
  Vue.http.get(url, headers).then(handleData, errorCallback)
}

export function repopulateExploration (mapContext, initialMap, scorecard) {
  const discovered = mapContext.context.getImageData(0, 0, mapContext.screenWidth, mapContext.screenHeight).data
  let currentDiscoverables = 0
  for (var i = 0; i < (discovered.length / 4); i++) {
    if (!(discovered[(i * 4)] > 80 && discovered[(i * 4)] < 155 &&
        discovered[(i * 4) + 1] > 163 && discovered[(i * 4) + 1] < 203 &&
        discovered[(i * 4) + 2] > 163 && discovered[(i * 4) + 2] < 203)) {
      currentDiscoverables++
    }
  }
  if (currentDiscoverables > initialMap) currentDiscoverables = initialMap
  scorecard.percentageExplored = Math.round((currentDiscoverables / initialMap) * 100) + '%'
  scorecard.exploredArea.style.width = scorecard.percentageExplored
}

export function getGoal (mapData) {
  let goal = {
    x: 0,
    y: 0
  }

  for (var keyY in mapData) {
    if (mapData.hasOwnProperty(keyY)) {
      for (var keyX in mapData[keyY]) {
        if (mapData[keyY].hasOwnProperty(keyX)) {
          for (var property in mapData[keyY][keyX]) {
            if (mapData[keyY][keyX].hasOwnProperty(property)) {
              if (property === 'Goal') {
                goal.x = keyY.split('posY')[1]
                goal.y = keyX.split('posX')[1]
                return goal
              }
            }
          }
        }
      }
    }
  }
}
