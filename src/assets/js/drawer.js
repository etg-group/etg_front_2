export function drawBackground (mapContext) {
  let myGradient = mapContext.context.createLinearGradient(0, 0, 0, mapContext.screenHeight)
  myGradient.addColorStop(0, 'rgba(154,202,202,0.8)')
  myGradient.addColorStop(1, 'rgba(81,164,164,0.8)')
  mapContext.context.fillStyle = myGradient
  mapContext.context.fillRect(0, 0, mapContext.screenWidth, mapContext.screenHeight)
}
export function drawMap (mapContext, mapData) {
  for (var keyY in mapData) {
    if (mapData.hasOwnProperty(keyY)) {
      for (var keyX in mapData[keyY]) {
        if (mapData[keyY].hasOwnProperty(keyX)) {
          for (var property in mapData[keyY][keyX]) {
            if (mapData[keyY][keyX].hasOwnProperty(property)) {
              if (mapData[keyY][keyX][property] === 'Working' ||
                mapData[keyY][keyX][property] === 'Feature' ||
                mapData[keyY][keyX][property] === 'FeatureFound' ||
                mapData[keyY][keyX][property] === 'Broken' ||
                mapData[keyY][keyX][property] === 'BugFound' ||
                mapData[keyY][keyX][property] === true) {
                switch (property) {
                  case 'pathRight':
                    drawHorizontalLine(mapContext, keyY.split('posY')[1], keyX.split('posX')[1], mapData[keyY][keyX][property])
                    break
                  case 'pathDown' :
                    drawVerticalLine(mapContext, keyY.split('posY')[1], keyX.split('posX')[1], mapData[keyY][keyX][property])
                    break
                }
              }
            }
          }
        }
      }
    }
  }
}
export function getInitialMapData (mapContext, mapData) {
  drawBackground(mapContext)
  drawMap(mapContext, mapData)
  let initialDiscoverables = 0
  const discovered = mapContext.context.getImageData(0, 0, mapContext.screenWidth, mapContext.screenHeight).data
  for (var i = 0; i < (discovered.length / 4); i++) {
    if (!(discovered[(i * 4)] > 80 && discovered[(i * 4)] < 155 &&
        discovered[(i * 4) + 1] > 163 && discovered[(i * 4) + 1] < 203 &&
        discovered[(i * 4) + 2] > 163 && discovered[(i * 4) + 2] < 203)) {
      initialDiscoverables++
    }
  }
  return initialDiscoverables
}

export function drawGoal (mapContext, goal) {
  const Goal = {
    sw: mapContext.screenWidth - (mapContext.margin * 2),
    sh: mapContext.screenHeight - (mapContext.margin * 2),
    margin: mapContext.margin,
    size: mapContext.width / 10
  }

  goal.element.style.backgroundSize = (Goal.size + 'px ' + Goal.size + 'px')
  goal.element.style.width = (Goal.size + 'px')
  goal.element.style.height = (Goal.size + 'px')
  goal.element.style.left = ((Goal.sw / 12) * goal.pos.x + Goal.margin - (Goal.size / 2) + 'px')
  goal.element.style.top = ((Goal.sh / 12) * goal.pos.y + Goal.margin - (Goal.size / 2) + 'px')
}

export function drawHero (mapContext, hero) {
  var realX = hero.x * hero.percentageX
  var realY = hero.y * hero.percentageY

  mapContext.context.beginPath()
  mapContext.context.arc(realX, realY, hero.size, 0, 2 * Math.PI, false)

  var grd = mapContext.context.createRadialGradient(realX, realY, hero.size - 2, realX + 2, realY + 2, hero.size + 2)
  grd.addColorStop(0, 'black')
  grd.addColorStop(0.5, 'darkblue')
  grd.addColorStop(1, '#c3c3c3')

  mapContext.context.strokeStyle = grd
  mapContext.context.stroke()
  mapContext.context.beginPath()

  var direction = Math.atan2(hero.speedY, hero.speedX) * 180 / Math.PI

  mapContext.context.moveTo(
      (Math.cos(direction / (180 / Math.PI)) * hero.size) + realX,
      (Math.sin(direction / (180 / Math.PI)) * hero.size) + realY
  )
  mapContext.context.lineTo(
      (Math.cos((direction + 45) / (180 / Math.PI)) * hero.size) + realX,
      (Math.sin((direction + 45) / (180 / Math.PI)) * hero.size) + realY
  )
  mapContext.context.lineTo(
      (Math.cos(direction / (180 / Math.PI)) * (hero.size * 1.5)) + realX,
      (Math.sin(direction / (180 / Math.PI)) * (hero.size * 1.5)) + realY
  )
  mapContext.context.lineTo(
      (Math.cos((direction - 45) / (180 / Math.PI)) * hero.size) + realX,
      (Math.sin((direction - 45) / (180 / Math.PI)) * hero.size) + realY
  )

  mapContext.context.fillStyle = 'darkblue'
  mapContext.context.fill()
}

function drawHorizontalLine (mapContext, posY, posX, type) {
  mapContext.context.beginPath()
  mapContext.context.lineWidth = 5
  var lineX = (posX * (mapContext.width / (mapContext.lines - 1))) + mapContext.margin
  var lineY = (posY * (mapContext.height / (mapContext.lines - 1))) + mapContext.margin
  var lineLength = (mapContext.width / (mapContext.lines - 1))
  mapContext.context.strokeStyle = defineStrokeColor(mapContext, type, lineX, lineY, 'horizontal')

  mapContext.context.moveTo(lineX, lineY)

  if (type === 'Broken' || type === 'BugFound') {
    var breakSize = mapContext.height / mapContext.lines - 40
    mapContext.context.lineTo(lineX + (lineLength / 7), lineY + breakSize)
    mapContext.context.lineTo(lineX + (lineLength / 5), lineY - breakSize)
    mapContext.context.stroke()

    mapContext.context.beginPath()
    mapContext.context.moveTo(lineX + (lineLength / 1.5), lineY + breakSize)
    mapContext.context.lineTo(lineX + (lineLength / 1.2), lineY - breakSize)
  }

  mapContext.context.lineTo(lineX + lineLength, lineY)
  mapContext.context.stroke()
}

function drawVerticalLine (mapContext, posY, posX, type) {
  mapContext.context.beginPath()
  mapContext.context.lineWidth = 5
  var lineX = (posX * (mapContext.width / (mapContext.lines - 1))) + mapContext.margin
  var lineY = (posY * (mapContext.height / (mapContext.lines - 1))) + mapContext.margin
  var lineLength = (mapContext.height / (mapContext.lines - 1))
  mapContext.context.strokeStyle = defineStrokeColor(mapContext, type, lineX, lineY, 'vertical')

  mapContext.context.moveTo(lineX, lineY)

  if (type === 'Broken' || type === 'BugFound') {
    var breakSize = mapContext.height / mapContext.lines - 40
    mapContext.context.lineTo(lineX + breakSize, lineY + (lineLength / 7))
    mapContext.context.lineTo(lineX - breakSize, lineY + (lineLength / 5))
    mapContext.context.stroke()

    mapContext.context.beginPath()
    mapContext.context.moveTo(lineX + breakSize, lineY + (lineLength / 1.5))
    mapContext.context.lineTo(lineX - breakSize, lineY + (lineLength / 1.2))
  }

  mapContext.context.lineTo(lineX, lineY + lineLength)
  mapContext.context.stroke()
}

function defineStrokeColor (mapContext, type, lineX, lineY, dir) {
  var grad
  if (dir === 'vertical') {
    grad = mapContext.context.createLinearGradient(lineX - 2.5, lineY, lineX + 2.5, lineY)
  } else {
    grad = mapContext.context.createLinearGradient(lineX, lineY - 2.5, lineX, lineY + 2.5)
  }
  switch (type) {
    case 'Working':
      grad.addColorStop(0, 'black')
      grad.addColorStop(0.1, '#999')
      grad.addColorStop(0.3, 'black')
      grad.addColorStop(0.7, 'black')
      grad.addColorStop(1, '#fff')
      return grad
    case 'Feature':
      grad.addColorStop(0, 'blue')
      grad.addColorStop(0.1, '#999')
      grad.addColorStop(0.3, 'blue')
      grad.addColorStop(0.7, 'blue')
      grad.addColorStop(1, '#fff')
      return grad
    case 'FeatureFound':
      grad.addColorStop(0, 'black')
      grad.addColorStop(0.1, '#999')
      grad.addColorStop(0.3, 'black')
      grad.addColorStop(0.7, 'black')
      grad.addColorStop(1, '#fff')
      return grad
    case 'Broken':
      return 'red'
    case 'BugFound':
      return 'black'
  }
}
