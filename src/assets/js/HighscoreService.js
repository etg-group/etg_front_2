import Vue from 'vue'
import VueResource from 'vue-resource'

Vue.use(VueResource)

const url = process.env.HIGHSCORE_URL

export function getHighscores (errorCallback, successCallback) {
  function handleData (rawData) {
    const prettyData = prettifyData(rawData.data)
    successCallback(prettyData)
  }
  Vue.http.get(url).then(handleData, errorCallback)
}

export function prettifyData (rawData) {
  let prettyData = []
  for (let rawItem of rawData) {
    var datetime = new Date(rawItem.date)
    const prettyItem = {
      name: rawItem.name,
      score: rawItem.score,
      date: formatDate(datetime)
    }
    prettyData.push(prettyItem)
  }
  return prettyData
}

function formatDate (date) {
  var hours = date.getHours()
  var minutes = date.getMinutes()
  var ampm = hours >= 12 ? 'pm' : 'am'
  hours = hours % 12
  hours = hours === 0 ? 12 : hours // the hour '0' should be '12'
  minutes = minutes < 10 ? '0' + minutes : minutes
  var strTime = hours + ':' + minutes + ' ' + ampm

  const monthNames = [
    'January', 'February', 'March',
    'April', 'May', 'June', 'July',
    'August', 'September', 'October',
    'November', 'December'
  ]

  var day = date.getDate()
  var monthIndex = date.getMonth()
  var year = date.getFullYear()

  return day + ' ' + monthNames[monthIndex] + ' ' + year + ' ' + strTime
}

export function saveHighscore (player, score, errorCallback, successCallback) {
  const headers = {
    headers: {
      'name': player,
      'score': score.toString()
    }
  }
  Vue.http.put(url, null, headers).then(successHandler, errorHandler)

  function successHandler (response) {
    successCallback(response)
  }
  function errorHandler (error) {
    errorCallback(error)
  }
}
