export function setHistoryMask (mapContext, hero) {
  mapContext.context.save()
  var visited = hero.visitedPos
  mapContext.context.beginPath()
  for (var i = 0; i < visited.length; i++) {
    mapContext.context.arc(visited[i].x, visited[i].y, 60, 0, 2 * Math.PI, false)
    mapContext.context.closePath()
  }
  mapContext.context.clip()
}

export function setCurrentMask (mapContext, hero) {
  mapContext.context.save()
  mapContext.context.beginPath()
  mapContext.context.arc(hero.x, hero.y, 60, 0, 2 * Math.PI, false)
  mapContext.context.closePath()
  mapContext.context.clip()
}

export function clearMask (mapContext) {
  mapContext.context.restore()
}
