export function updateTimebox (scorecard, timeboxReached) {
  const passedMilliseconds = new Date() - scorecard.timeboxStart
  const passedSeconds = Math.floor(passedMilliseconds / 1000)
  scorecard.timeboxText = scorecard.timebox - passedSeconds + ' seconds'
  scorecard.timeboxArea.style.width = (scorecard.timebox - passedSeconds) / 30 * 100 + '%'

  if (scorecard.timebox <= passedSeconds) {
    timeboxReached()
  }
}
