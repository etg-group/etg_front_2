import { drawBackground, drawMap, drawGoal, drawHero } from '@/assets/js/drawer'
import { moveHero } from '@/assets/js/heroController'
import { setHistoryMask, setCurrentMask, clearMask } from '@/assets/js/maskMaker'
import { repopulateExploration } from '@/assets/js/MapmakerService'
import { pickupDiscoveries, getPickupScore } from '@/assets/js/discoveryService'
import { updateTimebox } from '@/assets/js/timeboxService'

export function PlayGame (mapContext, mapData, goal, hero, controls, initialMap, scorecard) {
  let triggerExploreCalculator = false
  let endReached = false
  let timeboxReached = false

  moveHero(mapContext, hero, controls, () => { triggerExploreCalculator = true })
  pickupDiscoveries(mapContext, mapData, goal, hero, scorecard, (end) => { endReached = true })
  updateTimebox(scorecard, (end) => { timeboxReached = true })

  drawBackground(mapContext)
  setHistoryMask(mapContext, hero)
  drawMap(mapContext, mapData)
  clearMask(mapContext)

  drawBackground(mapContext)
  setCurrentMask(mapContext, hero)
  drawMap(mapContext, mapData)
  clearMask(mapContext)

  drawGoal(mapContext, goal)
  drawHero(mapContext, hero)

  if (triggerExploreCalculator) repopulateExploration(mapContext, initialMap, scorecard)
  if (endReached || timeboxReached) determineScore(scorecard, getPickupScore())
  if (endReached) return 'Completed'
  if (timeboxReached) return 'Failed'
  return 'still playing...'
}

function determineScore (scorecard, discoveryScore) {
  let calculatedScore = 10
  calculatedScore += discoveryScore
  const percentageExplored = parseFloat(scorecard.percentageExplored) / 100
  calculatedScore *= percentageExplored
  scorecard.score = Math.round(calculatedScore)
}
