export function getHint () {
  const pickedHint = Math.round(Math.random() * (hintLib.length - 1))
  return hintLib[pickedHint]
}

const hintLib = [
  'Only pause the session if your house is on fire.',
  'RTFM',
  'Exploring different paths leads to higher scores',
  'Take your time, but make sure you reach your goal in the end',
  'Any remaining seconds will not result in a higher score',
  'The highest scores are achievable by not following the rules',
  'Don`t forget your coffee',
  'If you are feeling sad, say the words `beep-boop` three times out loud!',
  'Don`t worry, it will be over soon',
  'Exploratory Testing is a lot easier while listening to Christmas songs'
]
