import { pickupFeature, pickupBug, getScore } from '@/assets/js/Discoverables'
export function pickupDiscoveries (mapContext, mapData, goal, hero, scorecard, endReached) {
  var lineLengthHor = (mapContext.width / (mapContext.lines - 1))
  var lineLengthVer = (mapContext.height / (mapContext.lines - 1))

  var dist, distX1, distX2, distX, distY, distY1, distY2

  for (var keyY in mapData) {
    if (mapData.hasOwnProperty(keyY)) {
      for (var keyX in mapData[keyY]) {
        if (mapData[keyY].hasOwnProperty(keyX)) {
          for (var property in mapData[keyY][keyX]) {
            if (mapData[keyY][keyX].hasOwnProperty(property)) {
              if (mapData[keyY][keyX][property] === 'Feature' ||
                mapData[keyY][keyX][property] === 'Broken') {
                var posY = keyY.split('posY')[1]
                var posX = keyX.split('posX')[1]

                var lineX = (posX * (mapContext.width / (mapContext.lines - 1))) + mapContext.margin
                var lineY = (posY * (mapContext.height / (mapContext.lines - 1))) + mapContext.margin

                switch (property) {
                  case 'pathRight':
                    distX1 = Math.abs((hero.x * hero.percentageX) - lineX)
                    distX2 = Math.abs((hero.x * hero.percentageX) - (lineX + lineLengthHor))
                    distX = distX1 < distX2 ? distX1 : distX2
                    distY = Math.abs((hero.y * hero.percentageY) - lineY)
                    dist = Math.sqrt(Math.pow(distX, 2) + Math.pow(distY, 2))
                    break
                  case 'pathDown' :
                    distX = Math.abs((hero.x * hero.percentageX) - lineX)
                    distY1 = Math.abs((hero.y * hero.percentageY) - lineY)
                    distY2 = Math.abs((hero.y * hero.percentageY) - (lineY + lineLengthVer))
                    distY = distY1 < distY2 ? distY1 : distY2
                    dist = Math.sqrt(Math.pow(distX, 2) + Math.pow(distY, 2))
                    break
                }
                if (dist <= hero.size) {
                  if (mapData[keyY][keyX][property] === 'Feature') {
                    mapData[keyY][keyX][property] = 'FeatureFound'
                    pickupFeature(scorecard)
                  }
                  if (mapData[keyY][keyX][property] === 'Broken') {
                    mapData[keyY][keyX][property] = 'BugFound'
                    pickupBug(scorecard)
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  distX = Math.abs((hero.x * hero.percentageX) - (((mapContext.width / 12) * goal.pos.x) + (mapContext.width / 10)))
  distY = Math.abs((hero.y * hero.percentageY) - (((mapContext.height / 12) * goal.pos.y) + (mapContext.width / 10)))
  dist = Math.sqrt(Math.pow(distX, 2) + Math.pow(distY, 2))
  if (dist <= (((mapContext.width / 20)) + (hero.size / 2))) {
    endReached()
  }
}

export function getPickupScore () {
  return getScore()
}
