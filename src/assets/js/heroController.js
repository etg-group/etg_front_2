export function moveHero (mapContext, hero, controls, explorationTrigger) {
  setControlBasedSpeed(controls, hero)

  slowDown(hero)

  moveOnlyWhileOnMap(mapContext, hero)

  bounceBackIfNearingEdge(mapContext, hero)

  roundPosition(hero)

  storeVisitedPositions(hero, () => {
    explorationTrigger()
  })
}

function setControlBasedSpeed (controls, hero) {
  switch (controls.type) {
    case 'Keyboard':
      if (controls.movLeft) hero.speedX--
      if (controls.movRight) hero.speedX++
      if (controls.movUp) hero.speedY--
      if (controls.movDown) hero.speedY++
      break
    case 'Mouse':
      if (!(controls.mouseX === 0 && controls.mouseY === 0)) {
        hero.speedX = (controls.mouseX - hero.x) / 25
        hero.speedY = (controls.mouseY - hero.y) / 25
      }
      break
  }
}

function slowDown (hero) {
  hero.speedX /= hero.friction
  hero.speedY /= hero.friction
}

function moveOnlyWhileOnMap (mapContext, hero) {
  var pixelX = mapContext.context.getImageData(((hero.x * hero.percentageX) + hero.speedX) | 0, (hero.y * hero.percentageY) | 0, 1, 1).data
  if (!(pixelX[0] > 80 && pixelX[0] < 155 &&
      pixelX[1] > 163 && pixelX[1] < 203 &&
      pixelX[2] > 163 && pixelX[2] < 203)) {
    hero.x += hero.speedX
  }

  var pixelY = mapContext.context.getImageData((hero.x * hero.percentageX) | 0, ((hero.y * hero.percentageY) + hero.speedY) | 0, 1, 1).data
  if (!(pixelY[0] > 80 && pixelY[0] < 155 &&
      pixelY[1] > 163 && pixelY[1] < 203 &&
      pixelY[2] > 163 && pixelY[2] < 203)) {
    hero.y += hero.speedY
  }
}

function roundPosition (hero) {
  hero.x = Math.round(hero.x)
  hero.y = Math.round(hero.y)
}

function bounceBackIfNearingEdge (mapContext, hero) {
  let pixel
  pixel = mapContext.context.getImageData(((hero.x * hero.percentageX) + 1) | 0, (hero.y * hero.percentageY) | 0, 1, 1).data
  if (!(pixel[0] === 0 && pixel[1] === 0)) {
    hero.x--
  }
  pixel = mapContext.context.getImageData(((hero.x * hero.percentageX) - 1) | 0, (hero.y * hero.percentageY) | 0, 1, 1).data
  if (!(pixel[0] === 0 && pixel[1] === 0)) {
    hero.x++
  }
  pixel = mapContext.context.getImageData((hero.x * hero.percentageX) | 0, ((hero.y * hero.percentageY) + 1) | 0, 1, 1).data
  if (!(pixel[0] === 0 && pixel[1] === 0)) {
    hero.y--
  }
  pixel = mapContext.context.getImageData((hero.x * hero.percentageX) | 0, ((hero.y * hero.percentageY) - 1) | 0, 1, 1).data
  if (!(pixel[0] === 0 && pixel[1] === 0)) {
    hero.y++
  }
}

function storeVisitedPositions (hero, callback) {
  if ((Math.abs(hero.visitedPos[hero.visitedPos.length - 1].x - hero.x) >= 40) ||
    (Math.abs(hero.visitedPos[hero.visitedPos.length - 1].y - hero.y) >= 40)) {
    hero.visitedPos.push({
      x: hero.x,
      y: hero.y
    })
    callback()
  }
}
